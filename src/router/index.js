import Vue from "vue";
import VueRouter from "vue-router";
import { Login } from "@phamod/vue-lib/dist/vue-lib.common";
import store from "@/store/index";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: { name: "Login" }
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    props: {
      smartcardEndpoint: "https://localhost/card",
      loginEndpoint: process.env.VUE_APP_LOGIN_LOCATION
    } //'http://localhost:8000/api/v1/login'}
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/query",
    name: "Query",
    component: () =>
      import(/* webpackChunkName: "query" */ "@/components/SparqlQuery.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.name !== "Login" && !store.getters.getToken) {
    next({ name: "Login" });
  } else {
    next();
  }
});

export default router;
